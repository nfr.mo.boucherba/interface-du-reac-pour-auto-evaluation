# Interface du REAC pour auto évaluation
Développez une application web permettant de lister les compétences du REAC et de s'auto évaluer.

# Contexte du projet
Votre région, dans le cadre de l'école du numérique, vous demande de développer un outil permettant a un futur apprenant de s'auto évaluer, à partir du REAC.

Le site devra être composé de :

une page d'accueil présentant le programme de Développeur web / web mobile, ainsi que les compétences du REAC.
une page d'enregistrement et d'authentification.
une page pour chaque compétence, permettant pour chaque utilisateur enregistré, de cocher les critères de performance acquis, ainsi que d'ajouter un commentaire optionnel.
Les utilisateurs non authentifié n’ont pas accès aux pages des compétences, et devront être redirigé vers une page les invitant à s’enregistrer.
Afin de permettre une évolution ultérieure, le site devra être développé avec le framework node Express.


# Modalités pédagogiques
Par groupe de 3
En mode Agile (1 scrum master, ppp quotidien)
Les diagrammes et maquettes devront être validées par le formateur avant de commencer à coder.
Commit réguliers en respectant la spécification de Conventional Commits

# Critères de performance
Le site est développé avec node Express
Le site correspond aux maquettes graphiques
Le site est adaptatif (responsive desktop / mobile)
Le code correspond aux diagrammes
Les commits sont réguliers et explicites
Le développement du projet respecte les méthodes agiles

# Modalités d'évaluation
auto évaluation à l’aide de l’outil tout juste créé
évaluation du groupe avec le formateur

# Livrables
- Dépôt github
- Site en ligne
- Diagramme de cas d’utilisation (uml au format pdf)
- Diagramme de classes (uml au format pdf)
- Maquettes graphiques (pdf)
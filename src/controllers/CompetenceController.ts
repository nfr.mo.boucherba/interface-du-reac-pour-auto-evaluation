import { Request, Response } from "express-serve-static-core";

export default class CompetenceController 
{
    static competence(req: Request, res: Response): void
    {
        const db = req.app.locals.db;
        let id = req.params.id;
        const query = db.prepare('SELECT title_competence FROM COMPETENCE WHERE id = ?').all(id);
        const content = db.prepare('SELECT content_competence FROM COMPETENCE WHERE id = ?').all(id);
        const critere = db.prepare('SELECT content_critter,id,competence_id FROM CRITTER WHERE competence_id = ?').all(id);

        res.render('pages/competence', {
            title: 'Welcome to Competence page',
            title_competence: query,
            content_competence: content,
            critere: critere,
        })
    }
}

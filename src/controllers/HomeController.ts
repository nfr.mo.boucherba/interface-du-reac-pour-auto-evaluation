import { Request, Response } from "express-serve-static-core";
import { idText } from "typescript";

export default class HomeController
{
    static index(req: Request, res: Response): void
    {
        const db = req.app.locals.db;
        const query = db.prepare('SELECT title_competence,id FROM COMPETENCE WHERE id <= 4').all();
        const query2 = db.prepare('SELECT title_competence,id FROM COMPETENCE WHERE id > 4').all();
        res.render('pages/index', {
            title: 'Titre professionnel de développeur web et web mobile',
            comp: query,
            comp2: query2,
            
            
        });console.log(query);
    }

    static about(req: Request, res: Response): void
    {
        res.render('pages/about', {
            title: 'About',
        });
    }
}
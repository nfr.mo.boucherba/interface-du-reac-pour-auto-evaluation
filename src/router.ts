import { Application } from "express";
import HomeController from "./controllers/HomeController";
import CompetenceController from "./controllers/CompetenceController";
import CriteresController from "./controllers/CriteresController";


export default function route(app: Application)
{
    /** Static pages **/
    app.get('/', (req, res) =>
    {
        HomeController.index(req, res);
    });

    app.get('/about', (req, res) =>
    {
        HomeController.about(req, res);
    });

    app.get('/competence/:id',(req, res) => {
        CompetenceController.competence(req, res);
    });

    app.post('/critere', (req, res) =>{
        CriteresController.critere(req, res);
    });
}
